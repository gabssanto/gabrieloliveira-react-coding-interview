import TextField from '@mui/material/TextField';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

export const ContactDetailsPage: React.FC = () => {
  const { personId } = useParams();
  const [contacts, setContacts] = useState(undefined);
  const [contact, setContact] = useState(undefined);
  useEffect(() => {
    const _contacts = localStorage.getItem('contacts');
    const [contact] = JSON.parse(_contacts).filter(
      (_contact: any) => _contact.id === personId
    );
    setContacts(JSON.parse(_contacts));
    setContact(contact);
  }, []);

  useEffect(() => {
    return () => {
      if (contacts) localStorage.setItem('contacts', JSON.stringify(contacts));
    };
  }, [contacts]);

  const handleUpdateContact = (e: any, type: string) => {
    const updatedContacts = contacts.map((contact: { id: string }) => {
      if (contact.id !== personId) return contact;
      return {
        ...contact,
        [type]: e.target.value
      };
    });
    setContacts(updatedContacts);
  };

  return (
    <>
      <h1>This is the edit page</h1>
      {contact && (
        <>
          <TextField
            id="outlined-basic"
            label="Email"
            defaultValue={contact.email}
            onChange={e => handleUpdateContact(e, 'email')}
            variant="outlined"
          />
          <TextField
            id="outlined-basic"
            label="First Name"
            defaultValue={contact.firstName}
            onChange={e => handleUpdateContact(e, 'firstName')}
            variant="outlined"
          />
          <TextField
            id="outlined-basic"
            label="Last Name"
            defaultValue={contact.lastName}
            onChange={e => handleUpdateContact(e, 'lastName')}
            variant="outlined"
          />
        </>
      )}
    </>
  );
};
